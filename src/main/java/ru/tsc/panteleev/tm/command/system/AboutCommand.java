package ru.tsc.panteleev.tm.command.system;

import org.jetbrains.annotations.NotNull;

public class AboutCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "about";

    @NotNull
    public static final String DESCRIPTION = "Show developer info.";

    @NotNull
    public static final String ARGUMENT = "-a";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("Name: Sergey Panteleev");
        System.out.println("E-mail: spanteleev@t1-consulting.ru");
    }

}
